import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private http: HttpClient
  ) { }


  getAllUsers(): Observable<any> {
    return this.http.get(`${environment.BASE_URL}user/getAllUsers`);
  }

  getUserDetails(payload: object): Observable<any> {
    return this.http.post(`${environment.BASE_URL}user/getUserDetails`, payload);
  }

  createUser(payload: object): Observable<any> {
    return this.http.post(`${environment.BASE_URL}user/createUser`, payload);
  }

  updateUser(payload: object): Observable<any> {
    return this.http.put(`${environment.BASE_URL}user/updateUser`, payload);
  }

  deleteUser(payload: object): Observable<any> {
    return this.http.put(`${environment.BASE_URL}user/deleteUser`, payload);
  }

}
