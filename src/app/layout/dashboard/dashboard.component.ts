import { Component, OnInit, OnDestroy, TemplateRef, ViewChild, ViewChildren, ComponentFactoryResolver } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Subscription } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/globals';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  // @ViewChild('addModal') ;
  @ViewChild('addModal') addModalTemplate: TemplateRef<any>;
  @ViewChild('deleteModalTemplate') deleteModalTemplate: TemplateRef<any>;


  adduserSubscription$: Subscription;
  getAllUserSubscription$: Subscription;
  getUserDetailSubscription$: Subscription;
  updateUserSubscription$: Subscription;
  deleteUserSubscription$: Subscription;

  modalRef: BsModalRef;
  userForm: FormGroup;

  allUsers: object[] = [];
  cols = [
    { key: 'firstName', label: 'First Name' },
    { key: 'lastName', label: 'Last Name' },
    { key: 'email', label: 'Email' },
    { key: 'phoneNo', label: 'PhoneNo' },
    { key: 'profileImg', label: 'Profile Image' }
  ];

  constructor(
    private _userService: UserService,
    private modalService: BsModalService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.initUserForm();
    this.getAllUsers();
  }

  initUserForm() {
    this.userForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      lastName: ['', Validators.required],
      phoneNo: ['', [Validators.required, Validators.maxLength(10), Validators.maxLength(10), Validators.pattern(GlobalConstants.VALIDATORS['NUMBER'])]],
      firstName: ['', Validators.required]
    });
  }



  getAllUsers() {
    this.getAllUserSubscription$ = this._userService.getAllUsers().subscribe(
      (res) => {
        this.allUsers = [];
        if (res?.status) {
          this.allUsers = res.data;
        }
      },
      (err) => {

      }
    )
  }


  onCancelModal() {
    this.id = '';
    this.flag = '';

    if (this.modalRef) {
      this.modalRef.hide();
    }
    this.userForm.reset();
  }
  flag: string = '';
  id: string = '';

  openAddModal({ flag, item }) {
    this.flag = flag;
    this.id = item['_id']
    this.initUserForm();
    if (flag === 'edit') {
      this.setFormValues(item)
      this.modalRef = this.modalService.show(this.addModalTemplate);
    }
    if (flag === 'add') {
      this.modalRef = this.modalService.show(this.addModalTemplate);
    }
    if (flag === 'delete') {
      this.modalRef = this.modalService.show(this.deleteModalTemplate);
    }
  }

  setFormValues({ firstName, lastName, email, phoneNo, profileImg }) {
    this.userForm.controls['firstName'].setValue(firstName);
    this.userForm.controls['lastName'].setValue(lastName);
    this.userForm.controls['email'].setValue(email);
    this.userForm.controls['phoneNo'].setValue(phoneNo);
  }

  formData = new FormData();

  onSubmit() {
    if (this.flag === 'add') {

      let formControls = this.userForm.value;
      this.formData.append('firstName', formControls['firstName']);
      this.formData.append('lastName', formControls['lastName']);
      this.formData.append('email', formControls['email']);
      this.formData.append('phoneNo', formControls['phoneNo']);

      this.adduserSubscription$ = this._userService.createUser(this.formData).subscribe(
        (res) => {
          console.log(res);
          this.getAllUsers();
          this.onCancelModal()
        }, (err) => { }
      );
    } else {
      this.userForm.value['id'] = this.id;

      this.updateUserSubscription$ = this._userService.updateUser(this.userForm.value).subscribe(
        (res) => {
          this.getAllUsers();
          this.onCancelModal()
        }, (err) => { }
      );
    }

  }

  onDelete() {
    this.deleteUserSubscription$ = this._userService.deleteUser({ _id: this.id }).subscribe(
      (res) => {
        this.getAllUsers();
        this.onCancelModal()
      }
    );
  }

  handleFileUpload(event) {
    this.formData = new FormData();
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.formData.append('user', file, file['name']);
    }
  }

  ngOnDestroy() {
    if (this.getAllUserSubscription$) {
      this.getAllUserSubscription$.unsubscribe();
    } if (this.getUserDetailSubscription$) {
      this.getUserDetailSubscription$.unsubscribe();
    } if (this.deleteUserSubscription$) {
      this.deleteUserSubscription$.unsubscribe();
    } if (this.updateUserSubscription$) {
      this.updateUserSubscription$.unsubscribe();
    } if (this.adduserSubscription$) {
      this.adduserSubscription$.unsubscribe();
    }
  }
}
