import { Component, OnInit, Input, TemplateRef, Output, EventEmitter } from '@angular/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { GlobalConstants } from 'src/app/globals';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  @Input() data;
  @Input() columns;
  @Output() openAddModal = new EventEmitter<any>();


  constructor(

  ) { }

  ngOnInit(): void {
  }

  getImg(imgpath) {
    return `${environment.BASE_URL}${imgpath}`;
  }

  openModal(flag, item) {
    this.openAddModal.emit({ flag: flag, item: item });
  }


}
